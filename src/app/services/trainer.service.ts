import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { finalize, switchMap, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Trainer } from '../models/trainer.model';
import { SessionService } from './session.service';

const API_URL: string = environment.API_URL;
const API_KEY: string = environment.API_KEY;

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  public _isLoading: boolean = false;
  public _error: string = '';

  constructor(
    private readonly http: HttpClient,
    private sessionService: SessionService
  ) {}

  private findByUsername(username: string): Observable<Trainer[]> {
    return this.http.get<Trainer[]>(`${API_URL}?username=${username}`);
  }

  private createTrainer(username: string): Observable<Trainer> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': API_KEY,
    });
    return this.http.post<Trainer>(`${API_URL}`, { username }, { headers });
  }

  public authenticate(username: string, onSuccess: () => void): void {
    this._isLoading = true;

    this.findByUsername(username)
      .pipe(
        switchMap((trainers: Trainer[]) => {
          if (trainers.length) return of(trainers[0]);
          return this.createTrainer(username);
        }),
        tap((trainer: Trainer) => {
          this.sessionService.setTrainer(trainer);
        }),
        finalize(() => {
          this._isLoading = false;
        })
      )
      .subscribe(
        (trainer: Trainer) => {
          onSuccess();
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
  }
}
