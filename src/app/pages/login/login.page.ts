import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { TrainerService } from '../../services/trainer.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly trainerService: TrainerService,
    private readonly sessionService: SessionService
  ) {}

  ngOnInit() {
    if (this.sessionService.trainer !== undefined) {
      this.router.navigate(['trainer']);
    }
  }

  get _isLoading(): boolean {
    return this.trainerService._isLoading;
  }

  onSubmit(loginForm: NgForm): void {
    const { username } = loginForm.value;
    this.trainerService.authenticate(username, async () => {
      await this.router.navigate(['trainer']);
    });
  }
}
