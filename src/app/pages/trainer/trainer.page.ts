import { Component } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage {
  constructor(private readonly sessionService: SessionService) {}

  get trainer(): Trainer | undefined {
    return this.sessionService.trainer;
  }
}
