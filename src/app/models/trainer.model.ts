import { Pokemon } from './pokemon.model';
export interface Trainer {
  id: number;
  pokemon: Pokemon[];
  username: string;
}
