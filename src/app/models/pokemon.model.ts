export interface Pokemon {
  avatar: string;
  id: number;
  name: string;
  url: string;
}
